require_relative 'lib/gitlab_kramdown/version'

Gem::Specification.new do |spec|
  spec.name          = 'gitlab_kramdown'
  spec.version       = GitlabKramdown::VERSION
  spec.authors       = ['Gabriel Mazetto']
  spec.email         = ['brodock@gmail.com']

  spec.summary       = %q{GitLab Flavored Kramdown}
  spec.description   = %q{GitLab Flavored Markdown extensions on top of Kramdown markup. Tries to be as close as possible to existing extensions.}
  spec.homepage      = 'https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 3.0')
  spec.rubygems_version = '3.4.13'

  spec.metadata['homepage_uri']    = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri']   = "#{spec.homepage}/blob/main/CHANGELOG.md"
  spec.metadata['bug_tracker_uri'] = "#{spec.homepage}/-/issues"

  spec.files            = Dir['lib/**/*.rb'] + %w[LICENSE.txt README.md CHANGELOG.md]
  spec.bindir           = 'exe'
  spec.executables      = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths    = ['lib']

  spec.add_runtime_dependency 'kramdown', '~> 2.4.0'
  spec.add_runtime_dependency 'rouge', '~> 4.1.0'
  spec.add_runtime_dependency 'nokogiri', '~> 1.15.0'
  spec.add_runtime_dependency 'asciidoctor-plantuml', '= 0.0.16'

  spec.add_development_dependency 'rake', '~> 13.0.0'
  spec.add_development_dependency 'rdoc', '~> 6.5.0'
  spec.add_development_dependency 'bundler', '~> 2.4.0'
  spec.add_development_dependency 'simplecov', '~> 0.22.0'
  spec.add_development_dependency 'gitlab-styles', '~> 10.0.0'
  spec.add_development_dependency 'rspec', '~> 3.12.0'
  spec.add_development_dependency 'rspec_junit_formatter', '~> 0.6.0'
  spec.add_development_dependency 'benchmark-ips', '~> 2.12.0'
end
