# frozen_string_literal: true

module GitlabKramdown
  VERSION = '0.27.0'
end
