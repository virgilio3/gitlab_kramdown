# frozen_string_literal: true

module GitlabKramdown
  module Parser
    # Special GitLab References
    #
    # This parser implements any non context-specific reference as described
    # in the GitLab Flavored Markdown reference, except for labels which were
    # extracted into it's own module.
    #
    # @see GitlabKramdown::Parser::Label
    # @see https://docs.gitlab.com/ee/user/markdown.html#special-gitlab-references
    module Reference
      PATH_REGEX = /[a-zA-Z0-9_.][a-zA-Z0-9_\-.]+/.freeze

      NAMESPACE_FORMAT_REGEX = %r{
        (?:#{PATH_REGEX}[a-zA-Z0-9_-])
        (?<!\.git|\.atom)
        |[a-zA-Z0-9_]
      }x.freeze

      FULL_NAMESPACE_FORMAT_REGEX = %r{(#{NAMESPACE_FORMAT_REGEX}/)*#{NAMESPACE_FORMAT_REGEX}}.freeze
      ALWAYS_FULL_NAMESPACE_FORMAT_REGEX = %r{(#{NAMESPACE_FORMAT_REGEX}/)+#{NAMESPACE_FORMAT_REGEX}}.freeze

      NAMESPACE_EXTRACTION_PATTERN = /(?<namespace>#{ALWAYS_FULL_NAMESPACE_FORMAT_REGEX})\z/.freeze

      USER_GROUP_PATTERN = %r{
        #{Regexp.escape('@')}
        (?<user>#{FULL_NAMESPACE_FORMAT_REGEX})
      }x.freeze

      PROJECT_COMMIT_PATTERN = %r{
        #{Regexp.escape('@')}
        (?<commit>[a-f0-9]{7,40})
        (?!\.{3})
      }x.freeze

      PROJECT_COMMIT_DIFF_PATTERN = %r{
        #{Regexp.escape('@')}
        (?<commit_source>[a-f0-9]{7,40})
        \.{3}
        (?<commit_target>[a-f0-9]{7,40})
      }x.freeze

      PROJECT_ISSUE_PATTERN = %r{
        #{Regexp.escape('#')}
        (?<issue>[1-9][0-9]*)
      }x.freeze

      PROJECT_MERGE_REQUEST_PATTERN = %r{
        #{Regexp.escape('!')}
        (?<merge_request>[1-9][0-9]*)
      }x.freeze

      PROJECT_SNIPPET_PATTERN = %r{
        #{Regexp.escape('$')}
        (?<snippet>[1-9][0-9]*)
      }x.freeze

      def self.included(klass)
        klass.define_parser(:user_group_mention, USER_GROUP_PATTERN, '@')
        klass.define_parser(:commit_diff, PROJECT_COMMIT_DIFF_PATTERN, '@')
        klass.define_parser(:commit, PROJECT_COMMIT_PATTERN, '@')
        klass.define_parser(:issue, PROJECT_ISSUE_PATTERN, '#')
        klass.define_parser(:merge_request, PROJECT_MERGE_REQUEST_PATTERN, '\!')
        klass.define_parser(:snippet, PROJECT_SNIPPET_PATTERN, '\$')
      end

      def parse_user_group_mention
        start_line_number = @src.current_line_number
        @src.pos += @src.matched_size

        # should not start with anything that looks like project/namespace
        if @src.pre_match.match?(/[a-zA-Z0-9_-]\Z/)
          add_text(@src[0])
          return
        end

        href = "#{@options[:gitlab_url]}/#{@src[:user]}"

        el = Kramdown::Element.new(:a, nil, { 'href' => href }, location: start_line_number)
        add_text(@src[0], el)
        @tree.children << el
      end

      # Extract namespace after a reference has been found
      #
      # This method provides a workaround for the lack of lookbehind support
      # in Ruby StringScanner.
      #
      # We look at pre_match content (everything before the reference in the same line)
      # and extract the very last word in the string as long as it matches the
      # NAMESPACE_EXTRACTION_PATTERN.
      #
      # We then modify the last parsed tree element's value, removing the matched
      # namespace, so that we can process that as part of the reference code.
      #
      # @see NAMESPACE_EXTRACTION_PATTERN
      def extract_reference_namespace!
        data = @src.pre_match.match(NAMESPACE_EXTRACTION_PATTERN)

        if data
          namespace = data[:namespace]

          # Remove namespace from last parsed tree value
          redacted = @tree.children.last.value[0..-(namespace.size + 1)]
          @tree.children.last.value = redacted
        end

        namespace
      end

      def parse_commit
        start_line_number = @src.current_line_number
        @src.pos += @src.matched_size

        namespace = extract_reference_namespace!
        reference_text = "#{namespace}#{@src[0]}"

        # If we can't find a namespace we just add content as regular text and skip
        unless namespace
          add_text(@src[0])

          return
        end

        href = "#{@options[:gitlab_url]}/#{namespace}/commit/#{@src[:commit]}"

        el = Kramdown::Element.new(:a, nil, { 'href' => href }, location: start_line_number)
        add_text(reference_text, el)
        @tree.children << el
      end

      def parse_commit_diff
        start_line_number = @src.current_line_number
        @src.pos += @src.matched_size

        namespace = extract_reference_namespace!
        reference_text = "#{namespace}#{@src[0]}"

        # If we can't find a namespace we just add content as regular text and skip
        unless namespace
          add_text(@src[0])

          return
        end

        href = "#{@options[:gitlab_url]}/#{namespace}/compare/" \
               "#{@src[:commit_source]}...#{@src[:commit_target]}"

        el = Kramdown::Element.new(:a, nil, { 'href' => href }, location: start_line_number)
        add_text(reference_text, el)
        @tree.children << el
      end

      def parse_issue
        start_line_number = @src.current_line_number
        @src.pos += @src.matched_size

        namespace = extract_reference_namespace!
        reference_text = "#{namespace}#{@src[0]}"

        # If we can't find a namespace we just add content as regular text and skip
        unless namespace
          add_text(@src[0])

          return
        end

        href = "#{@options[:gitlab_url]}/#{namespace}/issues/#{@src[:issue]}"

        el = Kramdown::Element.new(:a, nil, { 'href' => href }, location: start_line_number)
        add_text(reference_text, el)
        @tree.children << el
      end

      def parse_merge_request
        start_line_number = @src.current_line_number
        @src.pos += @src.matched_size

        namespace = extract_reference_namespace!
        reference_text = "#{namespace}#{@src[0]}"

        # If we can't find a namespace we just add content as regular text and skip
        unless namespace
          add_text(@src[0])

          return
        end

        href = "#{@options[:gitlab_url]}/#{namespace}/merge_requests/#{@src[:merge_request]}"

        el = Kramdown::Element.new(:a, nil, { 'href' => href }, location: start_line_number)
        add_text(reference_text, el)
        @tree.children << el
      end

      def parse_snippet
        start_line_number = @src.current_line_number
        @src.pos += @src.matched_size

        namespace = extract_reference_namespace!
        reference_text = "#{namespace}#{@src[0]}"

        # If we can't find a namespace we just add content as regular text and skip
        unless namespace
          add_text(@src[0])

          return
        end

        href = "#{@options[:gitlab_url]}/#{namespace}/snippets/#{@src[:snippet]}"

        el = Kramdown::Element.new(:a, nil, { 'href' => href }, location: start_line_number)
        add_text(reference_text, el)
        @tree.children << el
      end
    end
  end
end
