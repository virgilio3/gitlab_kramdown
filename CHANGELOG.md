# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.27.0] - 2023-06-04

### Changed

- Update `nokogiri` and other Gem dependencies.

## [0.26.0] - 2023-04-20

### Fixed

- Fix changelog dates.
- Resolve RuboCop violations.

### Changed

- Update Ruby version to 3.2.2.
- Update Gem dependencies.

## [0.25.0] - 2023-02-13

### Changed

- Update Ruby version to 3.2.1.
- Set Ruby 3.0 as the minimum required version and stop testing Ruby 2.7.
- Update Gem dependencies.

## [0.24.0] - 2023-01-27

### Changed

- Update Ruby version to 3.0.5, and test against 3.0 and 3.2.
- Update Gem dependencies.

## [0.23.0] - 2022-11-30

### Fixed

- Instructions for releasing new versions
- Small fixes to CHANGELOG.md

### Changed

- Update Ruby version to 2.7.7 and update Gem dependencies.

## [0.22.0] - 2022-10-31

### Changed

- Relax Ruby requirement to be all 2.7.x versions.

## [0.21.0] - 2022-10-23

### Changed

- Update dependencies and opt-in to new RuboCop cops

## [0.20.0] - 2022-06-13

### Changed

- Update required Ruby version to 2.7.6 and update Gem dependencies.

## [0.19.0] - 2022-04-26

### Changed

- Update Bundler and other Gem dependencies.

## [0.18.0] - 2022-03-11

### Changed

- Update Gem dependencies.

## [0.17.0] - 2022-02-10

### Added

- Add support for making PlantUML images a clickable link

## [0.16.0] - 2022-01-24

### Fixed

- Namespaced References are correctly extracted when `autolink: false`

### Changed

- Update minimum supported Ruby to 2.7.5
- Labels are now converted to an HTML similar to GitLab
- Labels were extracted to its own parser module

## [0.15.0] - 2021-12-20

### Changed

- Update Gem dependencies.

## [0.14.0] - 2021-08-10

### Changed

- Update Gem dependencies. In particular, update Nokogiri to version 1.12.x.

## [0.13.0] - 2021-07-02

### Changed

- Update Gem dependencies

## [0.12.0] - 2021-03-22

### Changed

- Update Gem dependencies. In particular, Kramdown to version 2.3.1.

## [0.11.0] - 2021-01-25

### Changed

- Update Gem dependencies.
- Update minimum required Ruby to 2.7.x.

## [0.10.0] - 2020-08-27

- Update Gem dependencies

## [0.9.0] - 2019-10-17

### Fixed

- Fix very long IDs for headers that include links.

### Added

- Images should be clickable and linked to itself
- Clickable images behavior is configurable

## [0.8.0] - 2019-10-14

### Changed

- Revert "Fix very long IDs for headers that include links"

## [0.7.0] - 2019-10-11

### Added

- Add support for PlantUML

### Changed

- Ruby 2.4 support is dropped.
- Fix very long IDs for headers that include links.

## [0.6.0] - 2019-05-07

### Changed

- URL Autolinks are now disabled by default (you can re-enable with `autolink: true` configuration param)

### Added

- Added support for rendering Mermaid when their JS is available

## [0.5.0] - 2019-04-09

### Added

- Strikethrough support

## [0.4.2] - 2018-11-21

### Changed

- URL autolinks will not execute inside `[]()` or `[]` blocks
- Freezing `kramdown` version to allow only patchlevel changes

## [0.4.1] - 2018-11-02

### Added

- Codebase now have a benchmark utility to compare with plain Kramdown

### Changed

- Many performance improvements on existing RegExp rules
- Improve gemspec metadata and include only needed files on gem

## [0.4.0] - 2018-04-12

### Added

- URL auto-linking support

### Changed

- Fixed many inconsistencies in references autolink

## [0.3.0] - 2018-03-23

### Added

- Headers will include by default an anchor tag (you can disable with `linkable_headers: false`)

### Changed

- Fixed multiline blockquote delimiter
- GitLab URL is not customizable with `gitlab_url: 'http://yourcustomgitlab.com'`

## [0.2.0] - 2018-03-19

### Added

- Syntax highlighter uses `` ``` `` or `~~~`

### Changed

- Requires Ruby 2.4
- Requires rouge `~> 3.0`

## [0.1.0] - 2018-03-17

### Added

- The initial version of the Gem
- Special GitLab References
- Multiline Blockquote

[0.27.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.26.0...v0.27.0
[0.26.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.25.0...v0.26.0
[0.25.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.24.0...v0.25.0
[0.24.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.23.0...v0.24.0
[0.23.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.22.0...v0.23.0
[0.22.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.21.0...v0.22.0
[0.21.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.20.0...v0.21.0
[0.20.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.19.0...v0.20.0
[0.19.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.18.0...v0.19.0
[0.18.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.17.0...v0.18.0
[0.17.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.16.0...v0.17.0
[0.16.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.15.0...v0.16.0
[0.15.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.14.0...v0.15.0
[0.14.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.13.0...v0.14.0
[0.13.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.12.0...v0.13.0
[0.12.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.11.0...v0.12.0
[0.11.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.10.0...v0.11.0
[0.10.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.9.0...v0.10.0
[0.9.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.8.0...v0.9.0
[0.8.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.7.0...v0.8.0
[0.7.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.6.0...v0.7.0
[0.6.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.5.0...v0.6.0
[0.5.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.4.2...v0.5.0
[0.4.2]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/gitlab-org/ruby/gems/gitlab_kramdown/-/compare/15b5e4b46aa0e42974ec2e5ee36c68d97219736f...v0.1.0
